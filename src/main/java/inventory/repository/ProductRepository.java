package inventory.repository;

import inventory.model.Inventory;
import inventory.model.Part;
import inventory.model.Product;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import java.io.*;
import java.util.StringTokenizer;

public class ProductRepository {

    private static String filename = "alpaca/products.txt";
    private static final Logger logger = LogManager.getLogger(ProductRepository.class);
    private Inventory inventory;

    public ProductRepository(Inventory inventory){
        this.inventory=inventory;
        try {
            readProducts();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    public void readProducts() throws FileNotFoundException {
        ClassLoader classLoader = ProductRepository.class.getClassLoader();
        File file = new File(classLoader.getResource(filename).getFile());

        ObservableList<Product> listP = FXCollections.observableArrayList();
        try (BufferedReader br = new BufferedReader(new FileReader(file))) {
            String line = null;
            while((line=br.readLine())!=null){
                Product product=getProductFromString(line);
                if (product!=null)
                    listP.add(product);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        inventory.setProducts(listP);
    }

    private Product getProductFromString(String line){
        Product product=null;
        if (line==null|| line.equals("")) return null;
        StringTokenizer st=new StringTokenizer(line, ",");
        String type=st.nextToken();
        if (type.equals("P")) {
            int id= Integer.parseInt(st.nextToken());
            inventory.setAutoProductId(id);
            String name= st.nextToken();
            double price = Double.parseDouble(st.nextToken());
            int inStock = Integer.parseInt(st.nextToken());
            int minStock = Integer.parseInt(st.nextToken());
            int maxStock = Integer.parseInt(st.nextToken());
            String partIDs=st.nextToken();

            StringTokenizer ids= new StringTokenizer(partIDs,":");
            logger.trace(partIDs);
            ObservableList<Part> list= FXCollections.observableArrayList();
            while (ids.hasMoreTokens()) {
                String idP = ids.nextToken();
                Part part = inventory.lookupPart(idP);
                if (part != null) {
                    logger.trace(idP);
                    list.add(part);
                }
            }
            product = new Product(id, name, price, inStock, minStock, maxStock, list);
            product.setAssociatedParts(list);
            logger.trace(product);
        }
        return product;
    }

    public void writeAll() {

        ClassLoader classLoader = ProductRepository.class.getClassLoader();
        File file = new File(classLoader.getResource(filename).getFile());

        ObservableList<Product> products=inventory.getProducts();

        try (BufferedWriter bw = new BufferedWriter(new FileWriter(file))) {
            for (Product pr:products) {
                ObservableList<Part> list= pr.getAssociatedParts();
                int index=0;
                StringBuilder bld = new StringBuilder();
                bld.append(pr.toString()).append(",");
                while(index<list.size()-1){
                    bld.append(list.get(index).getPartId()).append(":");
                    index++;
                }
                if (index==list.size()-1)
                    bld.append(list.get(index).getPartId());
                bw.write(bld.toString());
                bw.newLine();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void addProduct(Product product){
        inventory.addProduct(product);
        writeAll();
    }

    public int getAutoProductId(){
        return inventory.getAutoProductId();
    }

    public ObservableList<Product> getAllProducts(){
        return inventory.getProducts();
    }

    public Product lookupProduct (String search){
        return inventory.lookupProduct(search);
    }

    public void updateProduct(int productIndex, Product product){
        inventory.updateProduct(productIndex, product);
        writeAll();
    }

    public void deleteProduct(Product product){
        inventory.removeProduct(product);
        writeAll();
    }

    public Inventory getInventory(){
        return inventory;
    }

    public void setInventory(Inventory inventory){
        this.inventory=inventory;
    }
}
