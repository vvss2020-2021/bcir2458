package inventory.repository;

import inventory.model.*;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import java.io.*;
import java.util.StringTokenizer;

public class PartRepository {

    private static String filename = "alpaca/parts.txt";
    private static final Logger logger = LogManager.getLogger(PartRepository.class);
    private Inventory inventory;

    public PartRepository(Inventory inventory){
        this.inventory=inventory;
        try {
            readParts();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    public void readParts() throws FileNotFoundException {
        ClassLoader classLoader = PartRepository.class.getClassLoader();
        File file = new File(classLoader.getResource(filename).getFile());
        ObservableList<Part> listP = FXCollections.observableArrayList();
        try ( BufferedReader br = new BufferedReader(new FileReader(file))) {
            String line = null;
            while((line=br.readLine())!=null){
                Part part=getPartFromString(line);
                if (part!=null)
                    listP.add(part);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        inventory.setAllParts(listP);
    }

    private Part getPartFromString(String line){
        Part item=null;
        if (line==null|| line.equals("")) return null;
        StringTokenizer st=new StringTokenizer(line, ",");
        String type=st.nextToken();
        if (type.equals("I")) {
            int id= Integer.parseInt(st.nextToken());
            inventory.setAutoPartId(id);
            String name= st.nextToken();
            double price = Double.parseDouble(st.nextToken());
            int inStock = Integer.parseInt(st.nextToken());
            int minStock = Integer.parseInt(st.nextToken());
            int maxStock = Integer.parseInt(st.nextToken());
            int idMachine= Integer.parseInt(st.nextToken());
            item = new InhousePart(id, name, price, inStock, minStock, maxStock, idMachine);
        }
        if (type.equals("O")) {
            int id= Integer.parseInt(st.nextToken());
            inventory.setAutoPartId(id);
            String name= st.nextToken();
            double price = Double.parseDouble(st.nextToken());
            int inStock = Integer.parseInt(st.nextToken());
            int minStock = Integer.parseInt(st.nextToken());
            int maxStock = Integer.parseInt(st.nextToken());
            String company=st.nextToken();
            item = new OutsourcedPart(id, name, price, inStock, minStock, maxStock, company);
        }
        return item;
    }

    public void writeAll() {

        ClassLoader classLoader = PartRepository.class.getClassLoader();
        File file = new File(classLoader.getResource(filename).getFile());


        ObservableList<Part> parts=inventory.getAllParts();

        try(BufferedWriter bw = new BufferedWriter(new FileWriter(file))) {
            for (Part p:parts) {
                logger.trace(p.toString());
                bw.write(p.toString());
                bw.newLine();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void addPart(Part part){
        inventory.addPart(part);
        writeAll();
    }

    public int getAutoPartId(){
        return inventory.getAutoPartId();
    }

    public ObservableList<Part> getAllParts(){
        return inventory.getAllParts();
    }

    public Part lookupPart (String search){
        return inventory.lookupPart(search);
    }

    public void updatePart(int partIndex, Part part){
        inventory.updatePart(partIndex, part);
        writeAll();
    }

    public void deletePart(Part part){
        inventory.deletePart(part);
        writeAll();
    }

    public Inventory getInventory(){
        return inventory;
    }

    public void setInventory(Inventory inventory){
        this.inventory=inventory;
    }
}
