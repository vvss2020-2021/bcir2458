package inventory.service;

import inventory.model.*;
import inventory.repository.PartRepository;
import inventory.repository.ProductRepository;
import javafx.collections.ObservableList;

public class InventoryService2 {

    private PartRepository partRepository;
    private ProductRepository productRepository;

    public InventoryService2(PartRepository partRepository, ProductRepository productRepository){

        this.partRepository = partRepository;
        this.productRepository = productRepository;
    }


    public void addInhousePart(String name, double price, int inStock, int min, int  max, int partDynamicValue){
        InhousePart inhousePart = new InhousePart(partRepository.getAutoPartId(), name, price, inStock, min, max, partDynamicValue);
        partRepository.addPart(inhousePart);
    }

    public void addOutsourcePart(String name, double price, int inStock, int min, int  max, String partDynamicValue){
        OutsourcedPart outsourcedPart = new OutsourcedPart(partRepository.getAutoPartId(), name, price, inStock, min, max, partDynamicValue);
        partRepository.addPart(outsourcedPart);
    }

    public void addProduct(String name, double price, int inStock, int min, int  max, ObservableList<Part> addParts){
        Product product = new Product(productRepository.getAutoProductId(), name, price, inStock, min, max, addParts);
        productRepository.addProduct(product);
    }

    public ObservableList<Part> getAllParts() {
        return partRepository.getAllParts();
    }

    public ObservableList<Product> getAllProducts() {
        return productRepository.getAllProducts();
    }

    public Part lookupPart(String search) {
        return partRepository.lookupPart(search);
    }

    public Product lookupProduct(String search) {
        return productRepository.lookupProduct(search);
    }

    public void updateInhousePart(int partIndex, int partId, String name, double price, int inStock, int min, int max, int partDynamicValue){
        InhousePart inhousePart = new InhousePart(partId, name, price, inStock, min, max, partDynamicValue);
        partRepository.updatePart(partIndex, inhousePart);
    }

    public void updateOutsourcedPart(int partIndex, int partId, String name, double price, int inStock, int min, int max, String partDynamicValue){
        OutsourcedPart outsourcedPart = new OutsourcedPart(partId, name, price, inStock, min, max, partDynamicValue);
        partRepository.updatePart(partIndex, outsourcedPart);
    }

    public void updateProduct(int productIndex, int productId, String name, double price, int inStock, int min, int max, ObservableList<Part> addParts){
        Product product = new Product(productId, name, price, inStock, min, max, addParts);
        productRepository.updateProduct(productIndex, product);
    }

    public void deletePart(Part part){
        partRepository.deletePart(part);
    }

    public void deleteProduct(Product product){
        productRepository.deleteProduct(product);
    }

}
