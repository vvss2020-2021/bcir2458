package inventory;

import inventory.model.Inventory;
import inventory.repository.InventoryRepository;
import inventory.repository.PartRepository;
import inventory.repository.ProductRepository;
import inventory.controller.MainScreenController;
import inventory.service.InventoryService;
import inventory.service.InventoryService2;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;


public class Main extends Application {
    private static final Logger logger = LogManager.getLogger(Main.class);

    @Override
    public void start(Stage stage) throws Exception {
        Inventory inventory = new Inventory();
        InventoryRepository repo= new InventoryRepository(inventory);
        InventoryService service = new InventoryService(repo);
        logger.trace(service.getAllProducts());
        logger.trace(service.getAllParts());
        FXMLLoader loader= new FXMLLoader(getClass().getResource("/fxml/MainScreen.fxml"));

        Parent root=loader.load();
        MainScreenController ctrl=loader.getController();
        ctrl.setService(service);

        Scene scene = new Scene(root);
        
        stage.setScene(scene);
        stage.show();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
    
}
