package validator;

public class InventoryException extends RuntimeException{
    // default constructor
    InventoryException() {    }

    // parametrized constructor
    InventoryException(String str) { super(str); }
}
