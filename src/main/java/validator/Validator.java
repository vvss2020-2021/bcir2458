package validator;

import inventory.model.Part;

public class Validator {
    public void validate(Part part){
        if(part.getPrice() < 0){
            throw new InventoryException("The price must be greater than 0.");
        }
        if(part.getInStock() < part.getMin() || part.getInStock() > part.getMax()){
            throw new InventoryException("In stock must be between the give bounds.");
        }
    }
}
