package inventory.service;

import inventory.model.Product;
import inventory.repository.InventoryRepository;
import javafx.collections.FXCollections;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyString;

@ExtendWith(MockitoExtension.class)
class InventoryServiceUnitTest {

    @Mock
    private InventoryRepository repository;

    @InjectMocks
    private InventoryService service;

    private static Product p1;

    @BeforeEach
    void setUp() {
        p1 = new Product(1, "celalalt picior", 111.2, 1, 1, 100, FXCollections.observableArrayList());
        MockitoAnnotations.initMocks(this);
        service = new InventoryService(repository);
        Mockito.when(repository.lookupProduct(anyString())).thenAnswer(an -> {
            if(an.getArgument(0).equals("1")){
                return p1;
            }
            return null;
        });
    }

    @AfterEach
    void tearDown() {

    }

    @Test
    @DisplayName("Unit testing - searching for 1 repo and should find it")
    @Tag("UnitTestingService")
    void lookUpProductValid() {
        assertNotEquals(null, repository.lookupProduct("1"));
    }

    @DisplayName("Unit testing - searching for ciorap and should NOT find it")
    @Tag("UnitTestingService")
    @Test
    void lookupProductInvalid() {
        assertNull(repository.lookupProduct("ciorap"));
    }
}