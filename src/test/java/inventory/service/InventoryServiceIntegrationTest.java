package inventory.service;

import inventory.model.Inventory;
import inventory.model.Product;
import inventory.repository.InventoryRepository;
import javafx.collections.FXCollections;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyString;

class InventoryServiceIntegrationTest {

    @Mock
    private Inventory inventory;

    @InjectMocks
    private InventoryRepository repository;

    private InventoryService service;

    private Product p1;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
        p1 = new Product(1, "celalalt picior", 111.2, 1, 1, 100, FXCollections.observableArrayList());
        repository = new InventoryRepository(inventory);
        service = new InventoryService(repository);
        Mockito.when(inventory.lookupProduct(anyString())).thenAnswer(an -> {
            if(an.getArgument(0).equals("1")){
                return p1;
            }
            return null;
        });
    }

    @Test
    @DisplayName("Integrate testing - searching for 1 repo and should find it")
    @Tag("IntegrateServiceAndRepo")
    void lookUpProductValid() {
        assertNotEquals(null, service.lookupProduct("1"));
    }

    @DisplayName("Integrate testing - searching for ciorap and should NOT find it")
    @Tag("IntegrateServiceAndRepo")
    @Test
    void lookupProductInvalid() {
        assertNull(service.lookupProduct("ciorap"));
    }
}