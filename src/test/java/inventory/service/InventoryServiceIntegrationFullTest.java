package inventory.service;

import inventory.model.Inventory;
import inventory.model.Product;
import inventory.repository.InventoryRepository;
import javafx.collections.FXCollections;
import org.junit.jupiter.api.*;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyString;

class InventoryServiceIntegrationFullTest {

    private Inventory inventory;

    private InventoryRepository repository;

    private InventoryService service;

    private Product p1;

    @BeforeEach
    void setUp() {
        p1 = new Product(1, "celalalt picior", 111.2, 1, 1, 100, FXCollections.observableArrayList());
        inventory = new Inventory();
        repository = new InventoryRepository(inventory);
        service = new InventoryService(repository);
        repository.addProduct(p1);
    }

    @AfterEach
    void tearDown() {
        repository.deleteProduct(p1);
    }

    @Test
    @DisplayName("Integrate testing - searching for 1 repo and should find it")
    @Tag("IntegrateServiceWithAll")
    void lookUpProductValid() {
        assertNotEquals(null, service.lookupProduct("1"));
    }

    @DisplayName("Integrate testing - searching for ciorap and should NOT find it")
    @Tag("IntegrateServiceWithAll")
    @Test
    void lookupProductInvalid() {
        assertNull(service.lookupProduct("ciorap"));
    }
}