package inventory.repository;

import inventory.model.*;
import javafx.collections.FXCollections;
import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.ArgumentsSource;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.ValueSource;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

class InventoryRepositoryTest {
    private static InventoryRepository repo1;
    private static Inventory repo;
    private static Part partValidECP, partInvalidECP, partBVA1, partBVA2, partBVA3, partBVA4;
    private static Product p1, p2, p3;
    private long size;

    private static Stream<Arguments> providePartsValidBVA(){
        return Stream.of(
                Arguments.of(partBVA2),
                Arguments.of(partBVA3)
        );
    }

    private static Stream<Arguments> providePartsInvalidBVA(){
        return Stream.of(
                Arguments.of(partBVA1),
                Arguments.of(partBVA4)
        );
    }

    @BeforeEach
    void setUp() {
        repo = new Inventory();
        repo1 = new InventoryRepository(new Inventory());
        partValidECP = new InhousePart(1, "part", 12.0, 2, 1, 12, 1);
        partInvalidECP = new InhousePart(2, "part2", -12.0, 2, 1, 12, 11);
        partBVA1 = new InhousePart(3, "part3", 12.0, 0, 1, 12, 2);
        partBVA2 = new InhousePart(4, "part4", 12.0, 2, 1, 12, 102);
        partBVA3 = new InhousePart(5, "part5", 12.0, 11, 1, 12, 18);
        partBVA4 = new InhousePart(6, "part6", 12.0, 13, 1, 12, 41);
        size = repo1.getAllParts().size();
        p1 = new Product(1, "celalalt picior", 111.2, 1, 1, 100, FXCollections.observableArrayList());
        p2 = new Product(2, "ceva1", 111.2, 1, 1, 100, FXCollections.observableArrayList());
        p3 = new Product(3, "papuc", 111.2, 1, 1, 100, FXCollections.observableArrayList());
        repo1.addProduct(p1);
        repo.addProduct(p1);
        repo1.addProduct(p2);
        repo.addProduct(p2);
        repo1.addProduct(p3);
        repo.addProduct(p3);
    }

    @AfterEach
    void tearDown() {
        PrintWriter writer = null;
        try {
            writer = new PrintWriter("C:\\Users\\Claudia\\Downloads\\02_Inventory\\02_Inventory\\target\\classes\\alpaca\\items.txt");
            writer.print("");
            writer.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        repo1.deleteProduct(p1);
        repo.removeProduct(p1);
        repo1.deleteProduct(p2);
        repo.removeProduct(p2);
        repo1.deleteProduct(p3);
        repo.removeProduct(p3);
    }

    @Test
    @DisplayName("ECP Valid pentru pret >= 0")
    @Tag("ECP")
    void addPartECPValid() {
        repo1.addPart(partValidECP);
        Assertions.assertEquals(size + 1, repo1.getAllParts().size());
    }

    @Test
    @DisplayName("ECP Invalid pentru pret < 0")
    @Tag("ECP")
    void addPartECPInvalid() {
        Exception exception = assertThrows(RuntimeException.class, () -> {
            repo1.addPart(partInvalidECP);
        });

        String expectedMessage = "The price must be greater than 0.";
        String actualMessage = exception.getMessage();

        assertTrue(actualMessage.contains(expectedMessage));
    }

    @ParameterizedTest
    @MethodSource("providePartsValidBVA")
    @DisplayName("BVA Valid pentru cantitate = min + 1, cantitate = max - 1")
    @Tag("BVA")
    void addPartValidBVA(Part part) {
        repo1.addPart(part);
        Assertions.assertEquals(size + 1, repo1.getAllParts().size());
    }

    @ParameterizedTest
    @MethodSource("providePartsInvalidBVA")
    @DisplayName("BVA Invalid pentru cantitate = min - 1, cantitate = max + 1")
    @Tag("BVA")
    void addPartInvalidBVA(Part part) {
        Exception exception = assertThrows(RuntimeException.class, () -> {
            repo1.addPart(part);
        });

        String expectedMessage = "In stock must be between the give bounds.";
        String actualMessage = exception.getMessage();

        assertTrue(actualMessage.contains(expectedMessage));
    }

    @Test
    @DisplayName("Coverage testing - searching for papuc")
    @Tag("Coverage")
    void lookupProductTestNameLookup(){
        assertNotEquals(null, repo.lookupProduct("papuc"));
    }

    @Test
    @DisplayName("Coverage testing - searching for 1")
    @Tag("Coverage")
    void lookupProductTestIdLookup(){
        assertNotEquals(null, repo.lookupProduct("1"));
    }

    @Test
    @DisplayName("Coverage testing - searching for ciorap")
    @Tag("Coverage")
    void lookupProductTestTristLookup(){
        assertNull(repo.lookupProduct("ciorap"));
    }

    @Test
    @DisplayName("Coverage testing - searching for ciorap in empty repo")
    @Tag("Coverage")
    void lookupProductTestRepoIsEmpty(){
        repo.removeProduct(p1);
        repo.removeProduct(p2);
        repo.removeProduct(p3);
        Product p =  new Product(0, null, 0.0, 0, 0, 0, null);
        Product pppp = repo.lookupProduct("ciorap");
        assertEquals(p.getProductId(), pppp.getProductId());
        assertEquals(p.getAssociatedParts(), pppp.getAssociatedParts());
        assertEquals(p.getName(), pppp.getName());
        assertEquals(p.getPrice(), pppp.getPrice());
        assertEquals(p.getMin(), pppp.getMin());
        assertEquals(p.getMax(), pppp.getMin());
        assertEquals(p.getInStock(), pppp.getInStock());
    }

    @Test
    @DisplayName("Unit testing - searching for 1 repo and should find it")
    @Tag("UnitTestingRepo")
    void lookUpProductValidRepo() {
        assertNotEquals(null, repo1.lookupProduct("1"));
    }

    @DisplayName("Unit testing - searching for ciorap and should NOT find it")
    @Tag("UnitTestingRepo")
    @Test
    void lookupProductInvalidRepo() {
        assertNull(repo1.lookupProduct("ciorap"));
    }
}